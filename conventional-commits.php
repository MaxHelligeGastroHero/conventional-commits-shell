<?php

const POSSIBLE_TYPES = [
    'build',
    'ci',
    'docs',
    'feat',
    'fix',
    'perf',
    'refactor',
    'style',
    'test',
    'visual',
    'translation',
];

const POSSIBLE_SCOPES = [
    'gastro-theme',
    'g24-theme',
    'gp-theme',
];

const MAGENTO_SCOPES = [];

if (!function_exists('readline')) {
    echo 'readline not installed' . PHP_EOL;
    exit(1);
}

echo "\e[92m";
echo '
  ______                  ______                  _           
 / _____)                / _____)                (_)_         
| /      ___  ____ _   _| /      ___  ____  ____  _| |_   ___ 
| |     / _ \|  _ \ | | | |     / _ \|    \|    \| |  _) /___)
| \____| |_| | | | \ V /| \____| |_| | | | | | | | | |__|___ |
 \______)___/|_| |_|\_/  \______)___/|_|_|_|_|_|_|_|\___|___/ ';
echo "\033[0m\n";

echo PHP_EOL;
echo PHP_EOL;

main();

function main () {
    do {
        $abort = false;
        $type = addType();
        $scope = addScope();
        readline_completion_function('reset_completion');
        $subject = addSubject();
        $body = addBody();
        $footer = addFooter();
        $message = buildCommitMessage($type, $scope, $subject, $body, $footer);
        echo "\e[33m";
        echo $message;
        echo "\033[0m\n";
        echo PHP_EOL;
        echo PHP_EOL;
        echo "\033[36mDo You want to commit? [Yes/No]\033[0m\n";
        echo PHP_EOL;
        echo "\e[34m";
        $line = readline(' >> ');
        echo "\033[0m\n";
        if(trim($line) == 'Yes' || trim($line) == 'y' || trim($line) == 'yes' || trim($line) == 'Y') {
            $command = "git commit -m '$message'";
            exec($command);
            $abort = true;
        } else {
            echo "\033[36mDo You Want to Reset?\033[0m\n";
            echo PHP_EOL;
            echo "\e[34m";
            $line = readline(' >> ');
            echo "\033[0m\n";
            if(!(trim($line) == 'Yes' || trim($line) == 'y' || trim($line) == 'yes' || trim($line) == 'Y')) {
                $abort = true;
            }
        }
    } while (!$abort);
}

function gitAdd() {
    $input = false;
    do {
        echo "\033[36mDo you want to add files to git? [Yes/No]\033[0m\n";
        echo PHP_EOL;
        $line = readline(' >> ');
        if(!(trim($line) == 'Yes' || trim($line) == 'y' || trim($line) == 'yes' || trim($line) == 'Y')) {
            $input = true;
        } else {
            $command = "git status";
            $res = shell_exec($command);
            $explodeArray = explode("\n", $res);
            $modifiedArray = [];

            foreach($explodeArray as $value) {
                if(is_numeric(strpos($value, 'modified:'))) {
                    $modifiedArray[] = $value;
                }
            }
            echo PHP_EOL;
            echo PHP_EOL;
            echo "\033[36mWhat Files do You want to stage?\033[0m\n";
            echo PHP_EOL;
            foreach($modifiedArray as $key => $value) {
                echo "\033[31m"."[".$key."]: ".trim($value)."\033[0m\n";
            }
            echo PHP_EOL;
            $line = readline(' >> ');
        }
    } while (!$input);
}

function addType () {
    readline_completion_function('type_completion');
    do {
        echo getLinePrompt('Type');
        echo PHP_EOL;
        echo "\e[34m";
        $line = readline(' >> ');
        echo "\033[0m\n";
        echo PHP_EOL;
        $isValidInput = isset($line) && strlen($line) > 0 && in_array(trim($line), POSSIBLE_TYPES);
        if(!$isValidInput) {
            echo PHP_EOL;
            echo getErrorString('Type');
            echo PHP_EOL;
            echo PHP_EOL;
        }
    } while(!$isValidInput);
    return trim($line);
}

function addScope () {
    readline_completion_function('scope_completion');
    do {
        echo getLinePrompt('Scope');
        echo PHP_EOL;
        echo "\e[34m";
        $line = readline(' >> ');
        echo "\033[0m\n";
        echo PHP_EOL;
        $isValidInput = isset($line) && strlen($line) > 0 && in_array(trim($line), POSSIBLE_SCOPES);
        if(!$isValidInput) {
            echo PHP_EOL;
            echo getErrorString('Scope');
            echo PHP_EOL;
            echo PHP_EOL;
        }
    } while(!$isValidInput);
    return trim($line);
}

function addSubject () {
    do {
        echo getLinePrompt('Subject');
        echo "\033[36mSubject has to be set\033[0m\n";
        echo PHP_EOL;
        echo "\e[34m";
        $line = readline(' >> ');
        echo "\033[0m\n";
        echo PHP_EOL;
        $isValidInput = isset($line) && strlen($line) > 0;
        if(!$isValidInput) {
            echo PHP_EOL;
            echo getErrorString('Subject');
            echo PHP_EOL;
            echo PHP_EOL;
        }
    } while(!$isValidInput);
    return trim($line);
}

function addBody() {
    echo getLinePrompt('Message');
    echo PHP_EOL;
    echo "\e[34m";
    $line = readline(' >> ');
    do {
        $line .= "\n";
        $lineToCompare = $line;
        readline_on_new_line();
        $line .= readline(' >> ');
    } while($lineToCompare != $line);
    echo "\033[0m\n";
    echo PHP_EOL;
    return trim($line);
}

function addFooter() {
    echo getLinePrompt('Footer');
    echo "\033[36mFooter has to contain a Ticket ID (TECH-XX)\033[0m\n";
    echo PHP_EOL;
    echo "\e[34m";
    $line = readline(' >> ');
    do {
        $line .= "\n";
        $lineToCompare = $line;
        readline_on_new_line();
        $line .= readline(' >> ');
        $inputIsValid = ($lineToCompare != $line  && is_numeric(strpos(trim($line), 'TECH-')));
        if(!$inputIsValid) {
            echo getErrorString('Footer');
        }
    } while($inputIsValid);
    echo "\033[0m\n";
    echo PHP_EOL;
    return trim($line);
}

function buildCommitMessage($type, $scope, $subject, $body, $footer) {
    $message = $type.'('.$scope.'): '.$subject;
    if(strlen($body) > 0) {
        $message .= "\n\n".$body;
    }
    if(strlen($footer) > 0) {
        $message .= "\n\n".$footer;
    }
    return $message;
}

function getLinePrompt($context) {
    return "\033[36m".'Enter a Commit '.$context.': '."\033[0m\n";
}

function getErrorString ($context) {
    return "\033[31m" . "Wrong Commit ".$context."! Enter A valid ".$context. "\033[0m";
}

function type_completion($input)
{
    $matches = [];
    if(strlen($input)) {
        foreach(POSSIBLE_TYPES as $type) {
            $pos = strpos($type, $input);
            if (isset($pos)) {
                $matches[] = $type;
            }
        }
    } else {
        return POSSIBLE_TYPES;
    }

    return $matches;
}

function scope_completion($input)
{
    $matches = [];
    if(strlen($input)) {
        foreach(POSSIBLE_SCOPES as $type) {
            $pos = strpos($type, $input);
            if (isset($pos)) {
                $matches[] = $type;
            }
        }
    } else {
        return POSSIBLE_SCOPES;
    }

    return $matches;
}

function reset_completion() {
    return [];
}

function confirmInput ($line) {
    do {
        echo "Are you sure you want to add this line: ";
        echo PHP_EOL;
        echo $line;
        echo PHP_EOL;
        $answere = readline(' >> ');
        echo PHP_EOL;
    } while(!isset($answere));
    if($answere == 'y' || $answere == 'Y' || $answere == 'yes' || $answere == 'Yes') {
        return true;
    } else {
        return false;
    }
}
